<?php

namespace App\Traits;

use App\Models\Expense;
use App\Models\TeamMate;

trait OweAmountTrait
{
    public function calculateExpense($team_mate_id)
    {
        // Get Team Mates Counts Who Has Expenses
        $totalTeamMembers = TeamMate::has('expenses')->count();

        if ($totalTeamMembers === 0) {
            return null; // Return Null If There Are No Team Members With Expenses
        }

        $totalExpenses = Expense::sum('expense_amount');
        $amountOwedPerMember = $totalExpenses / $totalTeamMembers;

        $ownExpense = Expense::where('team_mate_id', $team_mate_id)->sum('expense_amount');

        // Calculate Expense
        $calculatedExpense = ($ownExpense) ? round($ownExpense - $amountOwedPerMember) : null;

        return $calculatedExpense;
    }
}
