<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\OweAmountTrait;

class TeamMate extends Model
{
    use HasFactory, OweAmountTrait;

    protected $fillable = [
        'first_name',
        'last_name',
        'email'
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class, 'team_mate_id');
    }
}
