<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'team_mate_id',
        'expense_datetime',
        'expense_amount'
    ];

    protected $casts = [
        'expense_datetime' => 'datetime'
    ];

    public function team_mate()
    {
        return $this->belongsTo(TeamMate::class, 'team_mate_id');
    }
}
