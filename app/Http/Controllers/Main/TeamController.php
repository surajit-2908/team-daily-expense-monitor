<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\TeamMate;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    /**
     * Get Team List
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        // Fetching Data
        $teams = TeamMate::get();

        return view('pages.team', compact('teams'));
    }

    /**
     * Get Team Info
     * @param int $team_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeam($team_id)
    {
        // Fetching Data
        $team = TeamMate::find($team_id);

        // Return Response
        $status = $team ? "success" : "error";
        $statusCode = $team ? 200 : 404;

        return response()->json([
            "status" => $status,
            "data" => $team
        ], $statusCode);
    }

    /**
     * Save Or Update Team
     * @param \Illuminate\Http\Request $request
     * @param int $team_id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function saveTeam(Request $request, $team_id = null)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:team_mates,email' . ($team_id ? ",{$team_id}" : "")
        ]);

        DB::beginTransaction();
        try {
            // Find Or Create New
            $team = TeamMate::findOrNew($team_id);

            // Preparing The Data
            $team->fill($request->only(['first_name', 'last_name', 'email']));

            // Save The Data
            $team->save();

            // Commit To DB
            DB::commit();

            // Return Success Response
            return redirect()->route('team.index')->with([
                "message" => [
                    "result" => "success",
                    "msg" => "Team " . ($team_id ? 'Updated' : 'Saved') . " Successfully."
                ]
            ]);
        } catch (Exception $exception) {
            // Roll Back DB
            DB::rollBack();
            // Return Error Response
            return redirect()->back()->with([
                "message" => [
                    "result" => "error",
                    "msg" => $exception->getMessage()
                ]
            ]);
        }
    }
}
