<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\TeamMate;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * Calculate All Transactions And Make A Nested Array
     * @return array
     */
    private function calculateTransactions()
    {
        // Get Team Mates Who Expended, With Total Expenses
        $expenses = Expense::groupBy('team_mate_id')
            ->select('team_mate_id', DB::raw('SUM(expense_amount) as total_expenses'))
            ->orderByDesc('total_expenses')
            ->get();

        // Calculate Average Expense
        $totalTeamMates = $expenses->count();
        $averageExpenses = $expenses->sum('total_expenses') / $totalTeamMates;

        // Mapping Owed Amount
        $owedAmounts = $expenses->mapWithKeys(function ($expense) use ($averageExpenses) {
            $owedAmount = $expense->total_expenses - $averageExpenses;
            return [$expense->team_mate_id => $owedAmount];
        });

        $transactions = [];

        // Storing Transactions Of Borrower And Lander
        foreach ($owedAmounts as $teamMateId => $owedAmount) {
            if ($owedAmount > 0) {
                $lenders = $owedAmounts->filter(function ($owed) {
                    return $owed < 0;
                });

                foreach ($lenders as $id => $payAmount) {
                    $payAmount = abs($payAmount);
                    $owedAmount = $owedAmounts[$teamMateId];

                    if ($owedAmount > 0) {
                        $amount = ($owedAmount < $payAmount) ? $owedAmount : $payAmount;

                        // Storing Lander Data
                        $transactions[$teamMateId][] = [
                            'lender' => $id,
                            'amount' => $amount
                        ];

                        // Storing Boorrower Data
                        $transactions[$id][] = [
                            'borrower' => $teamMateId,
                            'amount' => $amount
                        ];

                        // Deducting Amount
                        $owedAmounts[$id] -= $amount;
                        $owedAmounts[$teamMateId] -= $amount;
                    }
                }
            }
        }

        // Return Response
        return $transactions;
    }

    /**
     * Getting Data Which Are Needed To Show On Home Page
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $expenses = Expense::orderBy('created_at')->get();
        $highestExpense = Expense::select('team_mate_id', DB::raw('SUM(expense_amount) as total_expenses'))
            ->groupBy('team_mate_id')
            ->orderByDesc('total_expenses')
            ->first();

        // Get Team Mates Who Has Expenses
        $teamMates = TeamMate::has('expenses')->get();
        $transactions = ($expenses->count()) ? $this->calculateTransactions() : [];

        // Getting Team Mates To Show The Transactions
        $transactionTeamMates = TeamMate::select('id', 'first_name', 'last_name')
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item->id => $item->getFullNameAttribute()];
            })
            ->all();

        return view('pages.index', compact('expenses', 'highestExpense', 'teamMates', 'transactions', 'transactionTeamMates'));
    }
}
