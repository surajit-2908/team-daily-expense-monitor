<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\TeamMate;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
    /**
     * Get Expense List And Team List
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        // Fetching Data
        $teams = TeamMate::get();
        $expenses = Expense::orderByDesc('created_at')->get();

        return view('pages.expense', compact('teams', 'expenses'));
    }

    /**
     * Get Expense Info
     * @param int $expense_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExpense($expense_id)
    {
        // Fetching Data
        $expense = Expense::find($expense_id);

        // Return Response
        $status = $expense ? "success" : "error";
        $statusCode = $expense ? 200 : 404;

        return response()->json([
            "status" => $status,
            "data" => $expense
        ], $statusCode);
    }


    /**
     * Save Or Update Expense, If Expense Amount Is Zero Then Not Saving Or Delete If Exists
     * @param \Illuminate\Http\Request $request
     * @param int $expense_id
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function saveExpense(Request $request, $expense_id = null)
    {
        $request->validate([
            'team_mate_id' => 'required',
            'expense_datetime' => 'required|date',
            'expense_amount' => 'required|numeric'
        ]);

        DB::beginTransaction();

        try {
            $expenseAmount = $request->input('expense_amount');

            // Check Whether Expense Amount Is Greater Than Zero
            if ($expenseAmount > 0) {
                // Find Or Create New
                $expense = Expense::findOrNew($expense_id);

                // Preparing The Data
                $expense->team_mate_id = $request->input('team_mate_id');
                $expense->expense_datetime = $request->input('expense_datetime');
                $expense->expense_amount = $expenseAmount;

                // Save The Data
                $expense->save();

                DB::commit();

                $message = "Expense " . ($expense_id ? 'Updated' : 'Saved') . " Successfully.";

                return redirect()->route('expense.index')->with([
                    "message" => [
                        "result" => "success",
                        "msg" => $message
                    ]
                ]);
            } elseif ($expense_id) {
                $response = $this->deleteExpense($expense_id);
                DB::commit();

                // Return Error Response For Zero Expense Amount
                return redirect()->back()->with($response);
            } else {
                return redirect()->back()->with([
                    "message" => [
                        "result" => "error",
                        "msg" => "Expense Amount Can Not Be Zero."
                    ]
                ]);
            }
        } catch (Exception $exception) {
            // Roll Back DB
            DB::rollBack();

            // Return Error Response
            return redirect()->back()->with([
                "message" => [
                    "result" => "error",
                    "msg" => $exception->getMessage()
                ]
            ]);
        }
    }


    /**
     * Delete Expense, If Exists
     * @param int $expense_id
     * @return string[][]
     */
    private function deleteExpense($expense_id)
    {
        // Fetch Data and Delete the Expense
        $expense = Expense::find($expense_id);

        if ($expense) {
            $expense->delete();

            // Return Success Response
            return [
                "message" => [
                    "result" => "success",
                    "msg" => "Expense Deleted Successfully."
                ]
            ];
        }

        // Return Error Response
        return [
            "message" => [
                "result" => "error",
                "msg" => "Expense Not Found."
            ]
        ];
    }
}
