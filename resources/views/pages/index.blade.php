@extends('layouts.layout')


@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container" style="text-align: center">
            <h1 class="display-4">Rs.{{ number_format($expenses->sum('expense_amount')) }} spent in {{ $expenses->count() }}
                transactions</h1>
            <p class="lead" style="max-width: 650px; margin: 1.5em auto 0">{{ $expenses->groupBy('team_mate_id')->count() }}
                team mates have spend Rs.{{ number_format($expenses->sum('expense_amount')) }} in the last
                {{ $expenses->count() ? ($expenses->first()->created_at->diffInDays(now()) ? $expenses->first()->created_at->diffInDays(now()) . ' days' : 'day') : '0 day' }}
                with {{ $expenses->count() }} transactions. Team mate with most spending is
                {{ $highestExpense ? $highestExpense->team_mate->getFullNameAttribute() : 'No One' }} with
                Rs.{{ $highestExpense ? $highestExpense->total_expenses : '0' }} spent.</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h3>Laravel Assignment</h3>
                        </div>
                        <div class="card-text">
                            <p>
                                The assignment is pretty straightforward. You need to make these three html pages dynamic.
                                On the Team page, user should be able to add and edit team members. Edit team member form
                                should show like add team member when clicking on the list item.
                            </p>
                            <p>
                                In Expenses, user should be able to add and edit expenses made by team members. If the
                                amount entered is 0 in any transaction while editing, the row should be removed from the
                                ledger. The ledger should be shown Chronologically, in descending order.
                            </p>
                            <p>
                                On the index.html page, the system should show who owes what amount based on the number of
                                users. We can assume that all expenses are to be equally divided among all team mates.
                            </p>
                            <p>
                                <b>
                                    Special Use Case. Think of what happens when some expenses are added in the system but a
                                    new team mate is added. It is not fair to divide the older transactions with the new
                                    team mate. What will you do to ensure we get the right amount for all teammates fairly.
                                </b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="margin: 2em auto">
        <div class="row">
            @foreach ($teamMates as $teamMate)
                <div class="col-lg-12" style="margin: 1em 0;">
                    <h2>{{ $teamMate->getFullNameAttribute() }}</h2>
                    <ul class="list-group">
                        @foreach ($transactions[$teamMate->id] ?? [] as $nestedTransaction)
                            <li
                                class="list-group-item {{ @$nestedTransaction['borrower'] ? 'bg-success text-white' : 'bg-danger text-white' }}">
                                @if (@$nestedTransaction['borrower'])
                                    {{ $transactionTeamMates[$nestedTransaction['borrower']] }} will pay
                                    {{ $transactionTeamMates[$teamMate->id] }}
                                    Rs.{{ round($nestedTransaction['amount']) }}
                                @else
                                    {{ $transactionTeamMates[$nestedTransaction['lender']] }} will receive
                                    Rs.{{ round($nestedTransaction['amount']) }} from
                                    {{ $transactionTeamMates[$teamMate->id] }}
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach

        </div>
    </div>
@endsection
