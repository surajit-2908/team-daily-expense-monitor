@extends('layouts.layout')


@section('content')
    <div class="container" style="margin: 2em auto">
        <div class="row">
            <div class="col-lg-6">
                <h1>Team</h1>
            </div>
            <div class="col-lg-6" style="text-align: right">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#team" aria-expanded="false"
                    aria-controls="collapseExample">
                    Add Teammate
                </button>
            </div>
        </div>
        <div class="collapse" id="team" style="margin-top: 1em">
            <div class="card card-body">
                <form action="{{ route('team.create') }}" method="POST" id="teamForm">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control" name="first_name">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" name="last_name">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label for="exampleInputPassword1">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-block">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="collapse" id="teamEdit" style="margin-top: 1em">
            <div class="card card-body">
                <form action="#" method="POST" id="teamFormUpdate">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control" id="firstName" name="first_name">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" id="lastName" name="last_name">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label for="exampleInputPassword1">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="list-group">
                    @forelse ($teams as $team)
                        <a href="#"
                            class="list-group-item list-group-item-action flex-column align-items-start team-edit"
                            data-id="{{ $team->id }}">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{ $team->getFullNameAttribute() }}</h5>
                                @if ($team->expenses->count())
                                    <small>last expense:
                                        {{ $team->created_at->diffInDays(now()) ? $team->created_at->diffInDays(now()) . ' days ago' : 'today' }}
                                    </small>
                                @endif
                            </div>
                            @if ($team->calculateExpense($team->id) > 0)
                                <small>{{ $team->first_name }} has to pay
                                    Rs.{{ $team->calculateExpense($team->id) }}</small>
                            @elseif ($team->calculateExpense($team->id) < 0)
                                <small>{{ $team->first_name }} should receive
                                    Rs.{{ abs($team->calculateExpense($team->id)) }}</small>
                            @endif
                        </a>
                    @empty
                        <a href="javascript:void(0)"
                            class="list-group-item list-group-item-action flex-column align-items-start">No
                            data available</a>
                    @endforelse

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // Shared validation configuration
        let validationConfig = {
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                first_name: "Please enter first name",
                last_name: "Please enter last name",
                email: "Please provide a valid email address"
            },
            submitHandler: function(form) {
                form.submit();
            }

        };

        // Apply Validation To Add Expense Form
        $('#teamForm').validate(validationConfig);

        // Apply Validation To Update Expense Form
        $('#teamFormUpdate').validate(validationConfig);
    </script>

    <script>
        $(document).ready(function() {
            $(".team-edit").on('click', function() {
                let team_id = $(this).data('id');
                let url = "{{ route('team.info', ':slug') }}".replace(":slug", team_id);
                let form_url = "{{ route('team.update', ':slug') }}".replace(":slug", team_id);

                $.get(url, function(response) {
                    if (response.status === "success") {
                        $('#firstName').val(response.data.first_name);
                        $('#lastName').val(response.data.last_name);
                        $('#email').val(response.data.email);

                        $('#teamFormUpdate').attr('action', form_url);
                        $('#teamEdit').show();
                    } else {
                        alert('Something went wrong');
                    }
                });
            });
        });
    </script>
@endpush
