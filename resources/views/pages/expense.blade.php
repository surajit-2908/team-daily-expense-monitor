@extends('layouts.layout')


@section('content')
    <div class="container" style="margin:2em auto">
        <div class="row">
            <div class="col-lg-6">
                <h1>Team Expenses</h1>
            </div>
            <div class="col-lg-6" style="text-align: right">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#expense"
                    aria-expanded="false" aria-controls="collapseExample">
                    Add an Expense
                </button>
            </div>
        </div>
        <div class="collapse" id="expense" style="margin-top: 1em">
            <div class="card card-body">
                <form action="{{ route('expense.create') }}" method="POST" id="expenseForm">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group" id="teamMate">
                                <label for="teamMate">Select User</label>
                                <select class="form-control" name="team_mate_id">
                                    @foreach ($teams as $team)
                                        <option value="{{ $team->id }}">{{ $team->getFullNameAttribute() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="expenseDate">Expense Date</label>
                                <input type="datetime-local" class="form-control" name="expense_datetime"
                                    placeholder="Expense Date" max="{{ date('Y-m-d\TH:i:s', strtotime('+12 hours')) }}">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="expenseAmount">Expense Amount</label>
                                <input type="number" class="form-control" name="expense_amount"
                                    placeholder="Expense Amount" min="1">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label for="exampleInputPassword1">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-block">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="collapse" id="expenseEdit" style="margin-top: 1em">
            <div class="card card-body">
                <form action="#" method="POST" id="expenseFormUpdate">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group" id="teamMate">
                                <label for="teamMate">Select User</label>
                                <select class="form-control" name="team_mate_id" id="teamMateId">
                                    @foreach ($teams as $team)
                                        <option value="{{ $team->id }}">{{ $team->getFullNameAttribute() }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="expenseDate">Expense Date</label>
                                <input type="datetime-local" class="form-control" name="expense_datetime" id="expenseDate"
                                    placeholder="Expense Date" max="{{ date('Y-m-d\TH:i:s', strtotime('+12 hours')) }}">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="expenseAmount">Expense Amount</label>
                                <input type="number" class="form-control" name="expense_amount" id="expenseAmount"
                                    placeholder="Expense Amount">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label for="exampleInputPassword1">&nbsp;</label>
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Expense by</th>
                    <th scope="col">Date</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($expenses as $key => $expense)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $expense->team_mate->getFullNameAttribute() }}</td>
                        <td>{{ date('d/m/Y g:ia', strtotime($expense->expense_datetime)) }}</td>
                        <td>Rs.{{ $expense->expense_amount }}</td>
                        <td><button type="button" class="btn btn-light expense-edit"
                                data-id="{{ $expense->id }}">Edit</button></td>
                    </tr>

                @empty
                    <tr>
                        <td colspan="5">No data available</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

@push('scripts')
    <script>
        // Shared validation configuration
        let validationConfig = {
            rules: {
                team_mate_id: {
                    required: true
                },
                expense_datetime: {
                    required: true
                },
                expense_amount: {
                    required: true,
                    number: true
                }
            },
            messages: {
                team_mate_id: "Please select a user",
                expense_date: "Please enter expense date",
                expense_amount: {
                    required: "Please enter expense amount",
                    number: "Expense amount must be number"
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        };

        // Apply Validation To Add Expense Form
        $('#expenseForm').validate(validationConfig);

        // Apply Validation To Update Expense Form
        $('#expenseFormUpdate').validate(validationConfig);
    </script>

    <script>
        $(document).ready(function() {
            $(".expense-edit").on('click', function() {
                let form_url = "";
                let expenseDatetime = "";
                let formattedDatetime = "";

                let expense_id = $(this).data('id');
                let url = "{{ route('expense.info', ':slug') }}".replace(":slug", expense_id);

                $.get(url, (response) => {
                    if (response.status === "success") {
                        expenseDatetime = response.data.expense_datetime;
                        formattedDatetime = expenseDatetime.slice(0,
                            16); // Extract the datetime part without the timezone

                        $('#teamMateId').val(response.data.team_mate_id);
                        $('#expenseDate').val(formattedDatetime);
                        $('#expenseAmount').val(response.data.expense_amount);

                        form_url = "{{ route('expense.update', ':slug') }}".replace(":slug",
                            expense_id);
                        $('#expenseFormUpdate').attr('action', form_url);
                        $('#expenseEdit').show();
                    } else {
                        alert('Something went wrong');
                    }
                });

            });
        });
    </script>
@endpush
