<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{ route('index') }}">Team Daily Expense Monitor</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item {{ Route::is('index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('index') }}">Home
                    @if (Route::is('index'))
                        <span class="sr-only">(current)</span>
                    @endif
                </a>
            </li>
            <li class="nav-item {{ Route::is('team.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('team.index') }}">Team
                    @if (Route::is('team.index'))
                        <span class="sr-only">(current)</span>
                    @endif
                </a>
            </li>
            <li class="nav-item {{ Route::is('expense.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('expense.index') }}">Expenses
                    @if (Route::is('expense.index'))
                        <span class="sr-only">(current)</span>
                    @endif
                </a>
            </li>
        </ul>
    </div>
</nav>
