<div class="msg-div">
    {{-- Validation Error Message --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{-- Session Message --}}
    @if (Session::has('message'))
        @if (Session::get('message')['result'] == 'success')
            <p class="alert alert-success"><strong>{{ Session::get('message')['msg'] }}</strong></p>
        @else
            <p class="alert alert-danger"><strong>{{ Session::get('message')['msg'] }}</strong></p>
        @endif
    @endif
</div>
