<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Main\IndexController;
use App\Http\Controllers\Main\TeamController;
use App\Http\Controllers\Main\ExpenseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::prefix('team')->controller(TeamController::class)->group(function () {
    // Get All Team Mates
    Route::get('/', "index")->name('team.index');
    // Get Team
    Route::get('/get-team/{team_id}', "getTeam")->name('team.info');
    // Create Team
    Route::post('/create-team', "saveTeam")->name('team.create');
    // Update Team
    Route::patch('/update-team/{team_id}', "saveTeam")->name('team.update');
});

Route::prefix('expense')->controller(ExpenseController::class)->group(function () {
    // Get All Expenses
    Route::get('/', "index")->name('expense.index');
    // Get Expense
    Route::get('/get-expense/{expense_id}', "getExpense")->name('expense.info');
    // Create Expense
    Route::post('/create-expense', "saveExpense")->name('expense.create');
    // Update Expense
    Route::patch('/update-expense/{expense_id}', "saveExpense")->name('expense.update');
});
